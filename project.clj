(defproject microservicev1 "0.1.0-SNAPSHOT"
  :description "Microservice V0.1"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [midje "1.8.3"]
                 [http-kit "2.1.18"]
                 [ring/ring-core "1.5.0"]
                 [ring-middleware-format "0.7.0" :exclusions [ring]]
                 [ring/ring-json "0.4.0"]
                 [metosin/ring-http-response "0.8.0"]
                 [bidi "2.0.9"]
                 [cheshire "5.6.3"]
                 [com.novemberain/monger "3.0.2"]]
  :profiles {:uberjar {:aot :all
                       :main microservicev1.core}}
  :uberjar-name "microservicev1.jar")
