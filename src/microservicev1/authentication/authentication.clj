(ns microservicev1.authentication.authentication
  (:require [microservicev1.mongodb.mongo :as mongo])
  (:import [java.util UUID]
           [java.util.Date]))

(def max-session-seconds 3600)

(def sessions (atom {}))

(defn now [] (new java.util.Date))

(defn genereate-new-session [user]
  (let [new-session {user {:token   "dan"
                           ;:token   (UUID/randomUUID)
                           :created now}}]
    (swap! sessions merge new-session)
    new-session))

(defn end-session [session-token]
  (prn "Logging out : " session-token)
  (swap! sessions dissoc session-token))

(defn authenticate [user pass]
  (prn "Authenticating " user " - " pass)
  (if (mongo/authenticate user pass)
    (do (prn "Authentication SUCESSFUL")
        (genereate-new-session user))
    (do (prn "Authentication FAILED")
        nil)))

(defn authorization-successful []
  (prn "Authorization SUCESSFUL")
  true)

(defn authorization-failed []
  (prn "Authorization FAILED")
  false)

(defn session-expired? [{keys [:created] :as session-created}]
  (< (.getMillisOf (new java.util.Date))
     (+ (.getMillisOf session-created) max-session-seconds)))

(defn authorize [user session-token]
  (prn ">>>>>>>>>")
  (prn ">>>>>" user " - " session-token)
  (prn ">>>>>>>>> " @sessions)
  (let [session (@sessions user)]
    (prn ">>>>>" session)
    (if (and session
             (= session-token (session :token)))
      (authorization-successful)
      (authorization-failed))))

;(defn authorize [session-token]
;  (let [session-date (session-token @sessions session-token)]
;  (if (and session-date
;           (.before (new java.util.Date) (+ session-date max-session-seconds)))
;    (do (prn "Authorization SUCESSFUL")
;        true)
;    (do (prn "Authorization FAILED")
;        false))))