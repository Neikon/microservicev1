(ns microservicev1.mongodb.mongo
  (:require [monger.core :as monger]
            [monger.collection :as mc]))

(def db-name "login")

(def collection "login")

(defn authenticate [user pass]
  (let [connection  (monger/connect)
        db          (monger/get-db connection db-name)
        user-record (mc/find-maps db collection {:user user :pass pass})]
    (not (empty? user-record))))

