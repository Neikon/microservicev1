(ns microservicev1.core
  (:require [org.httpkit.server :as httpkit]
            [ring.middleware.format :refer [wrap-restful-format]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.session :refer [wrap-session]]
            [org.httpkit.server :as httpkit]
            [clojure.java.io :as io]
            [bidi.ring :as bidi]
            [microservicev1.routes :as routes])
  (:gen-class))

(def port 3000)

(def session-cookie {:cookie-attrs {:max-age 3600
                                    :secure true}})

(def app (-> routes/routes
             (bidi/make-handler)
             (wrap-keyword-params)
             (wrap-session session-cookie)
             (wrap-restful-format :formats [:edn :json])))

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))


(defn -main [& args]
  (prn "Initiating...")
  (httpkit/run-server (var app) {:port port :join? false})
  (prn "Listening on port " port))
