(ns microservicev1.routes
  (:require [org.httpkit.server :as httpkit]
            [ring.util.http-response :as resp]
            [cheshire.core :refer :all]
            [microservicev1.mongodb.mongo :as mongo]
            [microservicev1.authentication.authentication :as authentication]))

(defn serve-index [_]
  (prn "Serving index")
  (-> "<html><h1>Hello</h1></html>"
      (resp/ok)
      (resp/content-type "text/html; charset=UTF-8")))

(defn serve-ping [_]
  (-> "pong"
      (resp/ok)
      (resp/content-type "text/html; charset=UTF-8")))

(defn serve-login [{:keys [params] :as request}]
  (prn "Serving login for request: " request)
  (let [user (:username params)
        pass (:password params)]
    (if (authentication/authenticate user pass)
      (-> user
          (str "Session token: ")
          (resp/ok)
          (resp/content-type "text/html; charset=UTF-8"))
      (resp/unauthorized!))))

(defn serve-logout [{:keys [params] :as request}]
  (prn "Serving logout for request: " request)
  (let [session-token (:session-token params)]
    (authentication/end-session [session-token])
    (resp/ok)))

(defn serve-authorization [{:keys [params] :as request}]
  (prn "Serving authorization for request: " request)
  (let [user (:username params)
        session-token (:session-token params)]
    (if (authentication/authorize user session-token)
      (resp/ok)
      (resp/not-found))))

(def routes ["/" {""                   {:get serve-index}
                  ["ping/" :name]      {:get serve-ping}
                  ["login"]            {:post serve-login}
                  ["logout"]           {:post serve-logout}
                  ["authorize"]        {:post serve-authorization}}])


